import File from './Components/File';
import Message from './Components/Message';

function App() {
  return (
    <div className="App">
        <File/>
        <Message/>
        
    </div>
  );
}

export default App;
